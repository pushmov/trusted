<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'evidev' => array($vendorDir . '/eviweb/fuelphp-phpcs/src'),
    'Mailgun' => array($vendorDir . '/mailgun/mailgun-php/src'),
);
