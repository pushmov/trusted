<?=\View::forge('email/email_header');?>
<!-- START CENTERED WHITE CONTAINER -->
<table class="main">
	<!-- START MAIN CONTENT AREA -->
	<tr>
		<td class="wrapper">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<p>Hi <?=$first_name. ' '.$last_name; ?>,</p>
						<p><?=$user['first_name'].' '.$user['last_name'];?>, has invited you to join Trusted and Referred.</p>
						<p>Follow the link below to activate and create password for your Trusted account.</p>
						<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
							<tbody>
								<tr>
									<td align="left">
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td><a href="<?=\Uri::base(false)?>invite/<?=$token?>" target="_blank">Activate Account</a></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<p>Thanks</p>
					</td>
				</tr>
			</table>
        </td>
    </tr>
    <!-- END MAIN CONTENT AREA -->
</table>
<?=\View::forge('email/email_footer');?>