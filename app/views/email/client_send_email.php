<?=\View::forge('email/email_header');?>
<!-- START CENTERED WHITE CONTAINER -->
<table class="main">
	<!-- START MAIN CONTENT AREA -->
	<tr>
		<td class="wrapper">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<p>Hi <?=$name?>,</p>
						<p><?=$message?></p>
						<p>Thanks</p>
					</td>
				</tr>
			</table>
        </td>
    </tr>
    <!-- END MAIN CONTENT AREA -->
</table>
<?=\View::forge('email/email_footer');?>