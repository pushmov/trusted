<?=\View::forge('email/email_header');?>
<!-- START CENTERED WHITE CONTAINER -->
<table class="main">
	<!-- START MAIN CONTENT AREA -->
	<tr>
		<td class="wrapper">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<p>Hi there,</p>
						<p>Follow the link below to reset the password for your Trusted account.</p>
						<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
							<tbody>
								<tr>
									<td align="left">
										<table border="0" cellpadding="0" cellspacing="0">
											<tbody>
												<tr>
													<td><a href="<?=\Uri::create('recover/' . $token);?>" target="_blank">Reset Password</a></td>
												</tr>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
						<p>Please disregard this email if you have not requested password recovery.</p>
						<p>Thanks</p>
					</td>
				</tr>
			</table>
        </td>
    </tr>
    <!-- END MAIN CONTENT AREA -->
</table>
<?=\View::forge('email/email_footer');?>