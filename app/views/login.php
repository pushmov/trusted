<form class="form-basic form-login" method="post" action="/login/auth.json" novalidate>
	<fieldset>
		<div class="form-log-in-with-email">
			<div class="form-white-background">
				<div class="form-title-row">
					<h1>Log in</h1>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns">
						<label for="email">Email</label>
					</div>
					<div class="small-12 medium-9 columns">
						<input type="email" name="data[email]" id="email">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns">
						<label for="passwrd">Password</label>
					</div>
					<div class="small-12 medium-9 columns">
						<input type="password" name="data[passwrd]" id="passwrd">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-offset-3 columns">
						<input type="checkbox" name="data[remember]" id="remember">
						<span>Remember me</span>
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-offset-3 columns">
						<button type="button" class="button btn-primary" id="login">Log in</button>
					</div>
				</div>
			</div>
			<a href="/reset" class="form-forgotten-password">Forgotten password &middot;</a>
			<a href="/signup" class="form-create-an-account">Create an account &rarr;</a>
		</div>
		<div class="form-sign-in-with-social">
			<div class="form-row form-title-row">
				<span class="form-title">Sign in with</span>
			</div>
			<a href="#" class="form-google-button">Google</a>
			<a href="#" class="form-facebook-button">Facebook</a>
			<a href="#" class="form-twitter-button">Twitter</a>
		</div>
	</fieldset>
</form>