<form class="form-basic" method="post" action="/signup/submit.json">
	<div class="row small-12 medium-6 medium-centered column">
		<fieldset id="user_fields">
			<h4>My Details</h4>
			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_first_name" class="right inline">First Name <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-8 column">
					<input type="text" name="data[user][first_name]" id="user_first_name">
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_last_name" class="right inline">Last Name <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-8 column">
					<input type="text" name="data[user][last_name]" id="user_last_name">
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_phone" class="right inline">Phone <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-8 column">
					<input type="tel" name="data[user][phone]" id="user_phone">
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_website" class="right inline">Website <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-8 column">
					<input type="text" name="data[user][website]" id="user_website">
				</div>
			</div>
		</fieldset>
		<fieldset id="user_login">
			<h4>Login Details</h4>
			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_email" class="right inline">Email <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-8 column">
					<input type="email" name="data[user][email]" id="user_email">
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_passwrd" class="right inline">Password <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-7 column">
					<input type="password" name="data[user][passwrd]" id="user_passwrd">
				</div>
				<div class="small-12 medium-1 column">
					<span title="Show Password" class="postfix show-password"><i class="fa fa-eye fa-lg"></i></span>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-4 column">
					<label for="user_cpasswrd" class="right inline">Password Again <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-7 column">
					<input type="password" name="data[user][cpasswrd]" id="user_cpasswrd">
				</div>
				<div class="small-12 medium-1 column">
					<span title="Show Password" class="postfix show-password"><i class="fa fa-eye fa-lg"></i></span>
				</div>
			</div>
		</fieldset>
		<div class="btn-signup-group">
			<div class="row">
				<div class="small-12 column">
					<a href="/login" class="highlight tos">Already have an account? Login here →</a>
				</div>
			</div>
			<div class="row">
				<div class="small-12 column">
					<button type="button" class="button btn-primary" id="btn_signup">Signup</button>
					<button type="button" class="button btn-primary" id="btn_cancel">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</form>