<div class="clearfix">
	<h4 class="pull-left">Templates</h4>
	<button class="btn small btn-primary" id="create_new_template"><i class="fa fa-plus"></i>Add Template</button>
</div>
<table class="table table-striped" id="my_templates">
	<thead>
		<tr>
			<th>Title</th>
			<th>Description</th>
			<th>Width</th>
			<th>Height</th>
			<th>Status ID</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>