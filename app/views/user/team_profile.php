<form class="form-basic form-profile" method="post" action="/users/team/update.json">
	<div class="row">
		<ul class="breadcrumbs">
			<li><a href="/users/dashboard">Dashboard</a></li>
			<li><a class="current" href="#">Profile</a></li>
		</ul>
		
		<div class="small-12 columns">
			<div class="clearfix">
				<fieldset>
					<h4><?=$data['full_name']?> Profile</h4>
					<p><span class="astrix">*</span> All fields are required.</p>
					<div class="row">
						<input type="hidden" name="data[id]" value="<?=$data['id']?>">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">First name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="data[first_name]" id="first_name" value="<?=$data['first_name']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">Last name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="data[last_name]" id="last_name" value="<?=$data['last_name']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">Email <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="email" name="data[email]" id="email" value="<?=$data['email']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">Phone <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="tel" name="data[phone]" id="phone" value="<?=$data['phone']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">Website</label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="data[website]" id="website" value="<?=$data['website']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="" class="right inline">Cloud link</label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="data[cloud_link]" id="cloud_link" value="<?=$data['cloud_link']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-offset-4 columns">
							<button type="button" class="button btn-primary" id="btn_user_update">Update</button>
						</div>
					</div>
				</fieldset>
			</div>
		</div>
		
	</div>
</form>