<div class="row">
	<ul class="breadcrumbs">
		<li><a class="current" href="#">Dashboard</a></li>
	</ul>
	<div class="user-dashboard">
		<div>
			<div class="small-12 medium-12 large-6 columns">
				<div class="table-wrapper my-client"><?=$view['my_client']?></div>
				<div><button class="btn small btn-primary" id="btn_add_user"><i class="fa fa-plus"></i>Add User</button></div>
				<div class="table-wrapper my-team"><?=$view['my_team']?></div>
			</div>
			<div class="small-12 medium-12 large-6 columns">
				<div class="table-wrapper my-template"><?=$view['my_template']?></div>
				<div><button class="btn small btn-primary" id="btn_add_group"><i class="fa fa-plus"></i>Add Group</button></div>
				<div class="table-wrapper my-group"><?=$view['my_group']?></div>
			</div>
		</div>
	</div>
</div>