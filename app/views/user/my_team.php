<div class="clearfix">
	<h4 class="pull-left">My Team</h4>
</div>
<table class="table table-striped" id="my_team">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>