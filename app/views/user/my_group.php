<div class="clearfix">
	<h4 class="pull-left">My Field Groups</h4>
</div>
<table class="table table-striped" id="my_group">
	<thead>
		<tr>
			<th>Name</th>
			<th>Details</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>