<div class="clearfix">
	<h4 class="pull-left">Clients</h4>
	<button class="btn small btn-primary btn-add-client"><i class="fa fa-plus"></i>Add Client</button>
</div>
<table class="table table-striped" id="my_client">
	<thead>
		<tr>
			<th>Full Name</th>
			<th>Address</th>
			<th>City</th>
			<?php if($show_assoc) :?>
			<th>Assoc</th>
			<?php endif; ?>
			<th>Action</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>