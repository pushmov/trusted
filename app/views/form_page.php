<div class="row">
	<form action="" method="post" enctype="multipart/form-data" name="testform" id="testform" class="form-basic">
		<fieldset>
			<p>* All fields are required.</p>
			<div class="small-12 medium-6 column">
				<h4>Test Drive</h4>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="first_name" class="right inline">First Name <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input type="text" id="first_name" name="data[first_name]" value="<?=$data['first_name'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="last_name" class="right inline">Last Name: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input type="text" id="last_name" name="data[last_name]" value="<?=$data['last_name'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="company" class="right inline">Company: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[company]" type="text" id="company" size="40" value="<?=$data['company'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="phone_cell" class="right inline">Cell Phone: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input type="text" id="phone_cell" name="data[phone_cell]" value="<?=$data['phone_cell'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="email" class="right inline">Email: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[email]" type="text" id="email" size="40" value="<?=$data['email'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="website" class="right inline">Website: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[website]" type="text" id="website" size="40" value="<?=$data['website'];?>" />
					</div>
				</div>
			</div>
			<div class="small-12 medium-6 column">
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="client" class="right inline">Client's Name: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[client]" type="text" id="client" value="<?=$data['client'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="address" class="right inline">Property Address: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[address]" type="text" id="address" size="40" value="<?=$data['address'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="city" class="right inline">Property City: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<input name="data[city]" type="text" id="city" value="<?=$data['city'];?>" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-4 columns">
						<label for="photo" class="right inline">Property Photo: <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 columns">
						<div id="file_upload">Select File To Upload</div>
						<input name="img_file" type="hidden" id="img_file" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-offset-4 columns">
						<button type="button" id="testsubmit" class="button">Submit Now</button>
						<button type="reset" id="testreset" class="button">Reset</button>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>