<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?=$meta_title;?></title>
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css" media="screen" />
		<link href="/assets/css/style.css" rel="stylesheet" type="text/css" media="screen" />
		<script>var baseUrl = '<?=Uri::base(false);?>';</script>
		<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="/assets/js/modernizr.js"></script>
		<script src="/assets/js/foundation.min.js" type="text/javascript"></script>
		<script src="https://use.fontawesome.com/da6c768a0e.js"></script>
		<script src="/assets/js/jquery.mask.js" type="text/javascript"></script>
		<script src="/assets/js/app.js" type="text/javascript"></script>
		<?php echo \Asset::js($scripts, array(), null, false); ?>
	</head>
	<body>
		<header>
			<h1>header</h1>
		</header>

		<div class="main-content">
			<?=$content;?>
		</div>
		<footer>
			<h1>footer</h1>
		</footer>
	</body>
</html>