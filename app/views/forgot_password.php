<form id="form_reset" class="form-basic form-login form-forgot-password" method="post" action="/login/reset.json">
	<fieldset>
		<div class="form-log-in-with-email">
			<div class="form-white-background">
				<div class="form-title-row">
					<h1>Reset Password</h1>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns">
						<label for="email">Email</label>
					</div>
					<div class="small-12 medium-9 columns">
						<input type="email" name="data[email]" id="email">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-offset-3 columns">
						<button type="button" class="button btn-primary" id="reset">Reset</button>
					</div>
				</div>
			</div>
			<a href="/login" class="form-forgotten-password">Login &middot;</a>
			<a href="/signup" class="form-create-an-account">Create an account &rarr;</a>
		</div>
	</fieldset>
	
</form>