<div class="row">
	<ul class="breadcrumbs">
		<li><a href="/users/dashboard">Dashboard</a></li>
		<li><a class="current" href="#">Group</a></li>
	</ul>

	<div class="small-12 columns user-dashboard">
		<div class="clearfix">
			<form class="form-basic form-template" method="post" action="/users/group/update.json">
				<div class="template-header clearfix">
					<div class="small-12 columns">
						<div class="row">
							<div class="small-12 medium-4 columns">
								<label for="name" class="right inline">Group Name <span class="astrix">*</span></label>
							</div>
							<div class="small-12 medium-8 columns">
								<input type="text" name="data[name]" id="name" value="<?=$data['name']?>">
								<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
							</div>
						</div>
						<div class="row">
							<div class="small-12 medium-4 columns">
								<label for="details" class="right inline">Group Details <span class="astrix">*</span></label>
							</div>
							<div class="small-12 medium-8 columns">
								<textarea name="data[details]" id="details"><?=$data['details']?></textarea>
							</div>
						</div>
						
						<div class="row">
							<div class="small-12 medium-4 columns">
								<label for="status_id" class="right inline">Status</label>
							</div>
							<div class="small-12 medium-8 columns">
								<?=\Form::select('data[status_id]', $data['status_id'], \Model\Group::forge()->get_status(), array('id' => 'status_id'))?>
							</div>
						</div>
						
						<div class="row">
							<div class="small-offset-4 columns">
								<button type="button" id="btn_save_group" class="button">Save Group</button>
							</div>
						</div>
					</div>
					
					<div id="notification"></div>
				</div>
			</form>

			<div class="template-body">
				<div class="row">
					<button type="button" class="button primary" id="btn_add_field_group"><i class="fa fa-plus"></i>Add Field</button>
				</div>
				<div class="row">
					<div class="small-12 medium-12">
						<table class="table table-striped row-border" id="tbl_group_fields">
							<thead>
								<tr>
									<th>Title</th>
									<th>Type</th>
									<th>Content</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>