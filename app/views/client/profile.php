<form class="form-basic form-profile" method="post" action="/users/client/update.json">
	<div class="row">
		<ul class="breadcrumbs">
			<li><a href="/users/dashboard">Dashboard</a></li>
			<li><a class="current" href="#">Profile</a></li>
		</ul>

		<div class="small-12 medium-6 end column">
			<fieldset>
				<h4><?=$data['full_names']?> Profile</h4>
				<div class="row">
					<input type="hidden" name="data[id]" value="<?=$data['id']?>">
					<div class="small-12 medium-4 column">
						<label for="team_name" class="right inline">Full Name <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[full_names]" id="full_names" value="<?=$data['full_names']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_single_name" class="right inline">Single Name <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[single_names]" id="single_names" value="<?=$data['single_names']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_slogan" class="right inline">Address <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[address]" id="address" value="<?=$data['address']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_email" class="right inline">City <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[city]" id="city" value="<?=$data['city']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_website" class="right inline">ZIP <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[zip]" id="zip" value="<?=$data['zip']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_website" class="right inline">Email <span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-8 column">
						<input type="text" name="data[email]" id="email" value="<?=$data['email']?>">
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_website" class="right inline">Status</label>
					</div>
					<div class="small-12 medium-8 column">
						<?=\Form::select('data[status_id]', $data['status_id'], \Model\Client::forge()->get_status_array())?>
					</div>
				</div>

				<div class="row">
					<div class="small-12 medium-4 column">
						<label for="team_website" class="right inline">Property Photo</label>
					</div>
					<div class="small-12 medium-8 column">
						<div id="client_property" class="client-property"></div>
						<div id="file_upload" class="profile-picture-upload">Change Photo</div>
						<input name="img_file" type="hidden" id="img_file" />
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-offset-4 column">
						<button type="button" class="button btn-primary" id="btn_client_update">Update</button>
					</div>
				</div>

			</fieldset>
		</div>
	</div>
</form>