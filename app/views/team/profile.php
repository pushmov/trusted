<form class="form-basic form-profile" method="post" action="/team/profile/update.json">
	<div class="row">
		<ul class="breadcrumbs">
			<li><a href="/users/dashboard">Dashboard</a></li>
			<li><a class="current" href="#">Profile</a></li>
		</ul>
		<div class="small-12 columns">
			<div class="clearfix">
				<fieldset id="team_fields">
					<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
					<h4>My Team</h4>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="team_name" class="right inline">Team Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[name]" id="team_name" value="<?=$data['name']?>">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="team_single_name" class="right inline">Single Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[single_name]" id="team_single_name" value="<?=$data['single_name']?>">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="team_slogan" class="right inline">Slogan <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[slogan]" id="team_slogan" value="<?=$data['slogan']?>">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="team_email" class="right inline">Email</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[email]" id="team_email" value="<?=$data['email']?>">
						</div>
					</div>

					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="team_website" class="right inline">Website</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[website]" id="team_website" value="<?=$data['website']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-offset-3 columns">
							<button type="button" class="button btn-primary" id="btn_team_profile_update">Update</button>
						</div>
					</div>
				</fieldset>
			</div>	
		</div>
		<div class="small-12 medium-6 medium-pull-6 columns">
			
			
			
		</div>
	</div>
</form>