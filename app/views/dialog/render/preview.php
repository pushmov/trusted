<div class="modal-header"><?=$data['title']?>
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular form-preview" method="post" action="/render/confirm.json" id="form_confirm_render">
		<div class="row text-center">
			<div class="small-12 medium-12 columns">
				<input type="hidden" id="template_id" name="data[id]" value="<?=$data['id']?>">
				<input type="hidden" id="page_id" name="data[page]" value="<?=$data['page']?>">
				<button type="button" id="pdf_preview" class="btn btn-preview-download btn-primary"><i class="fa fa-search fa-lg"></i>Preview PDF</button>
				<button type="button" id="pdf_download" class="btn btn-preview-download btn-primary"><i class="fa fa-download fa-lg"></i>Download PDF</button>
			</div>
		</div>
	</form>
</div>