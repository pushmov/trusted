<div class="modal-header">Create Password</div>
<div class="modal-body text-center">
	<form class="form-basic" method="post" action="/users/profile/create_password.json" id="form_create_password">
		<div class="row">
			<fieldset>
				<div class="row">
					<div class="small-12 medium-3 columns">
						<label for="passwrd" class="right inline">Password<span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-9 columns">
						<input type="password" name="data[passwrd]" id="passwrd" value="">
					</div>
				</div>
				<div class="row">
					<div class="small-12 medium-3 columns">
						<label for="cpasswrd" class="right inline">Confirm password<span class="astrix">*</span></label>
					</div>
					<div class="small-12 medium-9 columns">
						<input type="password" name="data[cpasswrd]" id="cpasswrd" value="">
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<button type="button" class="button btn-primary" id="btn_create_password">Create Password</button>
					</div>
				</div>
			</fieldset>
		</div>
	</form>
</div>
