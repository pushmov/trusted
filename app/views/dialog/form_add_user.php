<div class="modal-header">Invite User
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/team/add_user.json" id="form_add_user">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="first_name" class="right inline">First Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[first_name]" id="first_name" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="last_name" class="right inline">Last Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[last_name]" id="last_name" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="email" class="right inline">Email <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="email" name="data[email]" id="email" value="">
						</div>
					</div>

				</div>
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="phone" class="right inline">Phone <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="tel" name="data[phone]" id="phone" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="website" class="right inline">Website</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[website]" id="website" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<button type="button" class="button btn-primary" id="btn_invite_user">Invite</button>
				</div>
				<div class="small-12 medium-6 columns" id="error_notification">

				</div>
			</div>
		</fieldset>
	</form>
</div>