<div class="modal-header">Add Field
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/add_page.json" id="form_add_field">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="location" class="right inline">Location</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[location]" id="location" value="<?=$data['location']?>" readonly="readonly">
							<input type="hidden" name="data[template_id]" id="template_id" value="<?=$data['template_id']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="pos_x" class="right inline">X-Coord</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="number" name="data[pos_x]" id="pos_x" value="<?=$data['pos_x']?>" placeholder="maximum value is 99999.9">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="pos_y" class="right inline">Y-Coord</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="number" name="data[pos_y]" id="pos_y" value="<?=$data['pos_y']?>" placeholder="maximum value is 99999.9">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="width" class="right inline">Width</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="number" name="data[width]" id="width" value="<?=$data['width']?>" placeholder="maximum value is 99999.9">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="height" class="right inline">Height</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="number" name="data[height]" id="height" value="<?=$data['height']?>" placeholder="maximum value is 99999.9">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="text_align" class="right inline">Align</label>
						</div>
						<div class="small-12 medium-9 columns">
							<?=\Form::select('data[text_align]', $data['text_align'], \Model\Field::forge()->text_align(), array('id' => 'text_align'))?>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="font_size" class="right inline">Font Size</label>
						</div>
						<div class="small-12 medium-9 columns">
							<?=\Form::select('data[font_size]', \Model\Field::FONT_SIZE_DEFAULT, \Model\Field::forge()->font_size(), array('id' => 'font_size'))?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="font_style" class="right inline">Font Style</label>
						</div>
						<div class="small-12 medium-9 columns">
							<?=\Form::select('data[font_style]', $data['font_style'], \Model\Field::forge()->font_style(), array('id' => 'font_style'))?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="font_color" class="right inline">Font Color</label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[font_color]" class="color" value="<?=$data['font_color']?>">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="type_id" class="right inline">Field Type</label>
						</div>
						<div class="small-12 medium-9 columns">
							<?=\Form::select('data[type_id]', $data['type_id'], \Model\Field::forge()->type_id(), array('id' => 'type_id'))?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="content" class="right inline">Field Content</label>
						</div>
						<div class="small-12 medium-9 columns">
							<textarea name="data[content]" id="content"><?=$data['content']?></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<button type="button" class="button btn-primary" id="btn_save_fields">Submit</button>
				</div>
				<div class="small-12 medium-6 columns" id="error_notification">
				</div>
			</div>
		</fieldset>
	</form>
</div>