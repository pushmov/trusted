<?php if($data['type_id'] == \Model\Field::T_GFIELD) : ?>
<div class="row">
	<div class="small-12 medium-4 columns">
		<label for="group" class="right inline">Group</label>
	</div>
	<div class="small-12 medium-8 columns">
		<?=\Form::select('data[group]', $data['group_id'], \Model\Group::forge()->find_groups($data), array('id' => 'group', 'data-field' => $data['content']))?>
	</div>
</div>
<?php endif;?>

<div class="row">
	<div class="small-12 medium-4 columns">
		<label for="content" class="right inline">Field Content</label>
	</div>
	<div class="small-12 medium-8 columns">
		<?=\Model\Field::forge()->field_content($data)?>
	</div>
</div>