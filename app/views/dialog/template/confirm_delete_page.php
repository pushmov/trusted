<div class="modal-header">Confirm Delete Page
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/delete_page.json" id="form_delete_page">
		<div class="row text-center">
			<p>Are you sure want to delete this page?<br>This will also remove fields and page image too. Continue?</p>
			<div class="small-12 medium-12 columns">
				<button type="button" id="btn_delete_page" class="btn btn-submit btn-primary danger">Continue</button>
			</div>
		</div>
	</form>
</div>