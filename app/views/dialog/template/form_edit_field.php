<div class="modal-header">Edit Field
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/template/save_field.json" id="form_save_field">
		<input type="hidden" name="data[id]" value="<?=$data['id']?>">
		<?=\View::forge('dialog/template/form_field', array('data' => $data));?>
	</form>
</div>