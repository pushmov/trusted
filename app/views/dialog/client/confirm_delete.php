<div class="modal-header">Confirm Delete
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/client/remove.json" id="form_remove_client">
		<div class="row text-center">
			<p>Are you sure want to delete this client record?</p>
			<input type="hidden" name="id" value="<?=$id?>">
			<div class="small-12 medium-12 columns">		
				<button type="button" class="btn btn-submit btn-primary danger">Remove</button>
			</div>
		</div>
	</form>
</div>