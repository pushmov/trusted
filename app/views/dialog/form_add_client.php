<div class="modal-header">Add Client
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/client/add_client.json" id="form_add_client">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="full_names" class="right inline">Full Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[full_names]" id="full_names" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="single_names" class="right inline">Single Name <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[single_names]" id="single_names" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="address" class="right inline">Address <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[address]" id="address" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="email" class="right inline">Email <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="email" name="data[email]" id="email" value="">
						</div>
					</div>
					
				</div>
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="city" class="right inline">City <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[city]" id="city" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="state" class="right inline">Country <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<?=\Form::select('country', $data['country'], \StateInfo::$country, array('id' => 'country'));?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="state" class="right inline">State <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns" id="state">
							<?=\StateInfo::get_state_form($data['country']);?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="zip" class="right inline">ZIP <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-9 columns">
							<input type="text" name="data[zip]" id="zip" value="">
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-3 columns">
							<label for="file_upload" class="right inline">Property Photo</label>
						</div>
						<div class="small-12 medium-9 columns">
							<div id="file_upload" class="profile-picture-upload">Select File To Upload</div>
							<input name="img_file" type="hidden" id="img_file" />
						</div>
					</div>
					
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<button type="button" class="button btn-primary" id="btn_add_client">Submit</button>
				</div>
				<div class="small-12 medium-6 columns text-right" id="form_error"></div>
			</div>
		</fieldset>
	</form>
</div>

<script>
	$('#file_upload').uploadFile({
		url: '/users/client/doupload.json',
		dragDrop: false,
		fileName: 'myfile',
		allowedTypes: 'jpg,jpeg,png',
		returnType: 'json',
		onSuccess: function(files,data,xhr) {
			$('#img_file').val(data[0]);
		}
	});
</script>