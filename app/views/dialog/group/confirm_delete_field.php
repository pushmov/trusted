<div class="modal-header">Confirm Delete Field
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/group/field_delete.json" id="form_delete_group_field">
		<div class="row text-center">
			<p>Are you sure want to delete this group field record?</p>
			<input type="hidden" name="data[id]" value="<?=$id;?>">
			<div class="small-12 medium-12 columns">
				<button type="button" id="btn_delete_group_field" class="btn btn-submit btn-primary danger">Delete</button>
			</div>
		</div>
	</form>
</div>