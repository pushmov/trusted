<div class="modal-header">Add Group
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/group/add.json" id="form_add_group">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="name" class="right inline">Name <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<input type="text" name="data[name]" id="name" value="<?=$data['name']?>">
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="details" class="right inline">Details <span class="astrix">*</span></label>
				</div>
				<div class="small-12 medium-9 columns">
					<textarea name="data[details]" id="details"><?=$data['details']?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="small-12 medium-3 columns">
					<label for="status" class="right inline">Status</label>
				</div>
				<div class="small-12 medium-9 columns">
					<?=\Form::select('data[status_id]', $data['status_id'], \Model\Group::forge()->get_status(), array('id' => 'status'));?>
				</div>
			</div>
			<div class="row">
				<div class="small-offset-3 medium-3 columns">
					<button type="button" id="btn_add_group" class="button">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>