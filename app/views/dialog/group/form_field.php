<div class="modal-header">Modify Group Field
	<a class="close-reveal-modal"><i class="fa fa-times modal-close" alt="Close" data-close></i></a>
</div>
<div class="modal-body text-center">
	<form class="form-basic form-regular" method="post" action="/users/group/edit_field.json" id="form_edit_field">
		<input type="hidden" name="data[id]" id="id" value="<?=$data['id']?>">
		<input type="hidden" name="data[group_id]" id="group_id" value="<?=$data['group_id']?>">
		<fieldset>
			<div class="row">
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="title" class="right inline">Title <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 columns">
							<input type="text" name="data[title]" id="title" value="<?=$data['title']?>" >
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="type_id" class="right inline">Type</label>
						</div>
						<div class="small-12 medium-8 columns">
							<?=\Form::select('data[type_id]', $data['type_id'], \Model\Group\Field::forge()->get_type(), array('id' => 'type_id'))?>
						</div>
					</div>
					<div id="upload_image"></div>
					<div class="row">
						<div class="small-offset-4 columns">
							<?php if($data['id'] != '') : ?>
							<button type="button" name="submit" class="button primary" id="btn_submit_edit_group_field">Submit</button>
							<?php else : ?>
							<button type="button" name="submit" class="button primary" id="btn_submit_add_group_field">Submit</button>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="small-12 medium-6 columns">
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="title" class="right inline">Status</label>
						</div>
						<div class="small-12 medium-8 columns">
							<?=\Form::select('data[status_id]', $data['status_id'], \Model\Group\Field::forge()->get_status(), array('id' => 'status_id'))?>
						</div>
					</div>
					<div class="row">
						<div class="small-12 medium-4 columns">
							<label for="content" class="right inline">Content <span class="astrix">*</span></label>
						</div>
						<div class="small-12 medium-8 end columns">
							<textarea name="data[content]" id="content"><?=$data['content']?></textarea>
						</div>
					</div>
				</div>
			</div>
		</fieldset>
	</form>
</div>