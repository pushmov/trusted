<?php if($data == \Model\Group\Field::T_IMAGE) :?>
<div class="row">
	<div class="small-12 medium-4 columns">
		<label for="type_id" class="right inline"><?=$txt?></label>
	</div>
	<div class="small-12 medium-8 columns">
		<div id="file_upload">Browse</div>
		<input name="data[img_file]" type="hidden" id="img_file" />
		<input name="data[data_id]" type="hidden" id="data_id" value="<?=$id?>"/>
		
		<div class="img-type-preview" id="img_type_prev">
			
		</div>
	</div>
</div>
<script>
	if($('#file_upload').length > 0) {
        $('#file_upload').uploadFile({
            url: '/users/group/doupload.json',
            dragDrop: false,
            fileName: 'myfile',
            allowedTypes: 'jpg,jpeg,png',
            returnType: 'json',
            onSuccess: function(files,data,xhr) {
                loadImageType($('#data_id').val());
				$('#img_file').val(data[0]);
            },
			dynamicFormData: function(){
				var data = {'id' : $('#data_id').val()};
				return data;
			},
			showProgress: true
        });
    }
    loadImageType($('#data_id').val());
	function loadImageType(id){
		$.get('/users/group/load_image_type.json', {id: id}, function(json){
            $('#img_type_prev').html('');
            if(json.success) {
                $('#img_type_prev').html('<img src="/'+json.image+'" width="120" height="120" />');
            }
        });
	}
</script>
<?php else :?>
<div></div>
<?php endif; ?>