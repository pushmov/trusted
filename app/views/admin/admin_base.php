<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Trusted and Referred Administration</title>
		<?=\Asset::css('foundation.min.css');?>
		<?php if (isset($styles)) echo $styles; ?>
		<?=\Asset::css('admin.css');?>

		<script>var baseUrl = '<?=Uri::base(false);?>';</script>
		<?=\Asset::js('jquery.min.js');?>
		<?php if (isset($scripts)) echo $scripts; ?>
	</head>

	<body>
		<div id="header" class="row">
			<div class="small-12 column">
				<h2>Administration</h2>
				<div id="toolbar"></div>
			</div>
		</div>
		<div class="row" id="content"><?=$content;?></div>
		<div id="footer" class="row">
			<div class="small-12 column"> Simple CMS </div>
		</div>
	</body>
</html>
