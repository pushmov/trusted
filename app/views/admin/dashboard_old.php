<div class="small-12 column">
<h2>Template Fields</h2>
</div>
<form action="<?=\Uri::create('admin')?>" method="post" id="data_form">
<div class="small-12 column">
<div class="small-6 column" style="float:left;" >
	<button id="btn_logout" class="button small">Logout</button>
	
		<div class="filter small-10" >			 
				Template: <?=\Form::select('id_template', $id_template, $cbo_templates, array('id' => 'id_template'));?>  Page: <?=\Form::select('page', $page, $cbo_pages, array('id' => 'page'));?> Page Image: <span id="fileupload" class="button">Select File To Upload</span>				
		</div>
	<div class="small-2" >
        <button id="" class="button small">Go to field</button>
    </div>
   
	<button id="btn_load" class="button small">Load</button><button id="btn_save" class="button small">Save</button>  <button id="btn_addpage" class="button small">Add Page</button><button id="btn_addfield" class="button small">Add Field</button> <button id="btn_preview" class="button small">Full Page Preview</button>   <select name="feild_ids" class="small-2"><option value=""> Select </option><option value="1" > 1 </option><option value="2"> 2 </option><option value="3" > 3 </option></select> <button id="btn_refresh" class="button small">Refresh</button>
</div>
 <div class="small-6 column " style="float:right">
    <iframe src="http://localhost/testdrive/admin/templates/dopreview/1/68" width="580" height="800"></iframe>
  </div>
	
		
		<div class="datalist">
			<table id="datalist" class="simple-edit" style="float:left">
				<thead>
					<tr>
						<th>Field</th>
						<th>Field Name</th>
						<th>Location</th>
						<th>X Coord</th>
						<th>Y Coord</th>
						<th>Width</th>
						<th>Height</th>
						<th>Font</th>
						<th>Font Size</th>
						<th>Font Style</th>
						<th>Font Color</th>
						<th>Text Align</th>
						<th>Text Content</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($fields as $field): ?>
						<tr id="id_<?=$field['id'];?>">
							<td><input name="data[f_<?=$field['id'];?>][id_field]" type="text" value="<?=$field['id_field'];?>" size="5" /></td>
							<td><select name="data[f_<?=$field['id'];?>][field_name]"><option value="Name 1" <?php if($field['field_name']=='Name 1'){ echo 'selected'; } ?>> Name 1 </option><option value="Name 2" <?php if($field['field_name']==1.0){ echo 'selected'; } ?>> Name 2 </option><option value="Name 3" <?php if($field['field_name']=='Name 3'){ echo 'selected'; } ?>> Name 3 </option></select></td>
							<td><input name="data[f_<?=$field['id'];?>][location]" type="text" value="<?=$field['location'];?>" size="2" /></td>
							<td><input name="data[f_<?=$field['id'];?>][pos_x]" type="text" value="<?=$field['pos_x'];?>" size="5" /></td>
							<td><input name="data[f_<?=$field['id'];?>][pos_y]" type="text" value="<?=$field['pos_y'];?>" size="5" /></td>
							<td><select name="data[f_<?=$field['id'];?>][width]"><option value="1.0" <?php if($field['width']==1.0){ echo 'selected'; } ?>> 1.0 </option><option value="2.0" <?php if($field['width']==2.0){ echo 'selected'; } ?>> 2.0 </option><option value="3.0" <?php if($field['width']==3.0){ echo 'selected'; } ?>> 3.0 </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][height]"><option value="1.0" <?php if($field['height']==1.0){ echo 'selected'; } ?>> 1.0 </option><option value="2.0" <?php if($field['height']==2.0){ echo 'selected'; } ?>> 2.0 </option><option value="3.0" <?php if($field['height']==3.0){ echo 'selected'; } ?>> 3.0 </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_id]"><option value="100" <?php if($field['font_id']==100){ echo 'selected'; } ?>> 100 </option><option value="200" <?php if($field['font_id']==200){ echo 'selected'; } ?>> 200 </option><option value="300" <?php if($field['font_id']==300){ echo 'selected'; } ?>> 300 </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_size]"><option value="10" <?php if($field['font_size']==10){ echo 'selected'; } ?>> 10 </option><option value="20" <?php if($field['font_size']==20){ echo 'selected'; } ?>> 20 </option><option value="300" <?php if($field['font_size']==30){ echo 'selected'; } ?>> 30 </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_style]"><option value="italic" <?php if($field['font_style']=='italic'){ echo 'selected'; } ?>> Italic </option><option value="normal" <?php if($field['font_style']=='normal'){ echo 'selected'; } ?>> Normal </option><option value="initial" <?php if($field['font_style']=='initial'){ echo 'selected'; } ?>> Initial </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][font_color]"><option value="white" <?php if($field['font_color']=='white'){ echo 'selected'; } ?>> White </option><option value="black" <?php if($field['font_color']=='black'){ echo 'selected'; } ?>> Black </option><option value="red" <?php if($field['font_color']=='red'){ echo 'selected'; } ?>> Red </option></select></td>
							<td><select name="data[f_<?=$field['id'];?>][text_align]"><option value="right" <?php if($field['text_align']=='right'){ echo 'selected'; } ?>> Right </option><option value="center" <?php if($field['text_align']=='center'){ echo 'selected'; } ?>> Center </option><option value="left" <?php if($field['text_align']=='left'){ echo 'selected'; } ?>> Left </option></select></td>
							<td><input name="data[f_<?=$field['id'];?>][content]" type="text" value="<?=$field['content'];?>" /></td>
						</tr>
						<?php endforeach; ?>
				</tbody>
			</table>
		</div>


 </div>
</form>
<script>
	var fid = 1;
	$(document).ready(function() {
		$('#btn_preview').click(function() {	
			$.post(baseUrl + 'admin/templates/preview.json', $('#data_form').serialize(), function(json) {				
				window.open(json.url, "pdfpreview");
			});
		});
		$('#btn_save').click(function() {
			$.post(baseUrl + 'admin/templates/save.json', $('#data_form').serialize(), function(json) {
				alert('Data Saved');
			});
		});
		$('#btn_load').click(function() {
			$('#data_form').submit();
		});
		$('#btn_addpage').click(function() {
			$('#page').val('new');
			$('#data_form').submit();
		});
		$('#btn_addfield').click(function() {
			var html = '<tr><td><input name="data[a'+fid+'][id_field]" type="text" size="5" /></td><td><select name="data[a'+fid+'][field_name]"><option value=""> Select </option><option value="Name 1"> Name 1 </option><option value="Name 2"> Name 2 </option><option value="Name 3"> Name 3 </option></select></td><td><input name="data[a'+fid+'][location]" type="text" size="2" /></td><td><input name="data[a'+fid+'][pos_x]" type="text" size="5" /></td><td><input name="data[a'+fid+'][pos_y]" type="text" size="5" /></td><td><select name="data[a'+fid+'][width]"><option value=""> Select </option><option value="1.0" > 1.0 </option><option value="2.0" > 2.0 </option><option value="3.0"> 3.0 </option></select></td><td><select name="data[a'+fid+'][height]"><option value=""> Select </option><option value="1.0" > 1.0 </option><option value="2.0" > 2.0 </option><option value="3.0"> 3.0 </option></select></td><td><select name="data[a'+fid+'][font_id]"><option value=""> Select </option><option value="100" > 100 </option><option value="200" > 200 </option><option value="300"> 300 </option></select></td><td><select name="data[a'+fid+'][font_size]"><option value=""> Select </option><option value="10" > 10 </option><option value="20" > 20 </option><option value="30"> 30 </option></select></td><td><select name="data[a'+fid+'][font_style]"><option value=""> Select </option><option value="italic" > Italic </option><option value="normal"> Normal </option><option value="initial" > Initial </option></select></td><td><select name="data[a'+fid+'][font_color]"><option value=""> Select </option><option value="white" > White </option><option value="black"> Black </option><option value="red" > Red </option></select></td><td><select name="data[a'+fid+'][text_align]"><option value=""> Select </option><option value="right" > Right </option><option value="center"> Center </option><option value="left" > Left </option></select></td>	<td><input name="data[a'+fid+'][content]" type="text" /></td></tr>';
			fid++;
			$('#datalist > tbody:last').append(html);
		});

		$('#fileupload').uploadFile({
			url: baseUrl + 'admin/templates/doupload.json',
			dragDrop: false,
			fileName: 'myfile',
			allowedTypes: 'jpg,jpeg',
			dynamicFormData: function() {
				var data = {template: $('#id_template').val(), page: $('#page').val()}
				return data;
			},
			returnType: 'json',
			onSuccess: function(json) {
				jQuery('#photo').val(json[0]);
			}
		});
		$('#btn_logout').click(function() {
			window.location = baseUrl + 'admin/login/logout';
		});
	});
</script>
<style>
.datalist {
    overflow-x: scroll;
    background: #fff;
    border-right: solid 8px #fff;
    border-bottom: solid 20px #fff;
    border-left: 2px solid #fff;
}
.button{
	    padding: .80em 0.80em;
    margin: 0 0.6rem 0.6rem 0;
}
</style>