<?php
namespace Model;

/**
* Groups together fields that a user can use in their templates
*/
class Group extends \Model_Crud {
	const S_NEW = 0;
	const S_ACTIVE = 1;
	const S_HOLD = 7;
	const S_ARCHIVE = 9;

	protected static $_table_name = 'groups';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected static $_defaults =	array('id' => null, 'name' => '', 'details' => '', 'created_by' => 0, 'created_at' => '', 'updated_by' => 0, 
		'updated_at' => '', 'status_id' => self::S_NEW);
	protected static $_mass_whitelist = array('id', 'name', 'details', 'created_by', 'created_at', 'updated_by', 'updated_at', 'status_id');
	protected static $_rules = array('name' => 'required', 'details' => 'required');
	protected static $_labels = array('name' => 'Group Name', 'details' => 'Group Details');
	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Archived');

	public function get_data(){
		return self::$_defaults;
	}
	public function get_status($val=null){
		if($val === null) {
			return $this->_status;
		}
		return isset($this->_status[$val]) ? $this->_status[$val] : $this->_status;
	}
	public function find_group($user) {
		$agent_id = $user->id;
		if ($user->parent_id != '0') {
			$agent_id = $user->parent_id;
		}
		return self::find_one_by_created_by($agent_id);
	}
	public function find_groups($params) {
		$data = array();
		$groups = self::find_by(array(
			'created_by' => $params['user_id'],
		));
		if ($groups !== null) {
			foreach($groups as $row) {
				$data[$row->id] = $row->name;
			}
		}
		return $data;
	}
	public function all_groups($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$sqlStatus = "(CASE ";
		foreach($this->_status as $val => $name){
			$sqlStatus .= " WHEN status_id = '{$val}' THEN '{$name}'";
		}
		$sqlStatus .= " END)";
		$aColumns = array('name', 'details', 'status');
		$sql = "SELECT SQL_CALC_FOUND_ROWS id,name,details,{$sqlStatus} AS status FROM ".self::$_table_name
			. " WHERE created_by = '{$params['user_id']}'";
		$data['recordsTotal'] = \DB::query($sql,  \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "name LIKE '%{$params['search']['value']}%'";
			$sql .= " OR status LIKE '%{$params['search']['value']}%'";
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY updated_at DESC";
		}
		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT " . (int)$params['start'] . ", " . (int)$params['length'];
		}
		
		$result = \DB::query($sql, \DB::SELECT)->execute();
		$aColumns[] = 'action';
		foreach ($result as $row) {
			$row['action']  = \Form::button('button', '<i class="fa fa-edit"></i>Edit', array('class' => 'button small btn-group-edit', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash"></i>Remove', array('class' => 'button small alert btn-group-remove', 'type' => 'button', 'data-id' => $row['id']));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}
	public function update_group($data){
		$response = array();
		if(isset($data['id'])) {
			$model = self::find_by_pk($data['id']);
			$model->set($data);
		} else {
			$model = self::forge($data);
		}
		$val = $model->validation();
		$val->add_callable('rules');
		if($model->validates()) {
			$model->save();
			$response['success']['message'] = '/users/group/edit/'.$model->id;
			$response['id'] = $model->id;
			$response['success']['notification'] = 'Group updated';
		} else {
			$response['error']['message'] = $val->error_message();
		}
		return $response;
	}
	public function delete_group($id){
		$response = array();
		$group = self::find_by_pk($id);
		if($group === null) {
			return true;
		}
		$groupfields = \Model\Group\Field::find_by_group_id($id);
		if(count($groupfields) > 0) {
			foreach($groupfields as $row) {
				$row->delete();
			}
		}
		$group->delete();
		$response['success'] = true;
		return $response;
	}
}