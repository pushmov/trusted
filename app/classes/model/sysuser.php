<?php
namespace Model;

class Sysuser extends \Model_Crud {
	const L_ADMIN = 1;
	const L_EDITOR = 2;
	const L_MODERATOR = 3;
	const L_USER = 10;
	const S_NEW = 0;
	const S_ACTIVE = 1;
	const S_INACTIVE = 2;

	const S_NEW = 0; // New not validated
	const S_ACTIVE = 1;
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived

	protected static $_table_name = 'sys_users';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';
	protected static $_updated_at = 'updated_at';

	protected $_data =	array('id' => NULL, 'email' => '', 'first_name' => '', 'last_name' => '', 'passwrd' => '',
		'created_at' => '', 'updated_at' => '', 'notify_comm' => 0, 'id_level' => self::L_USER, 'id_status' => self::S_NEW);
	protected $_status = array(self::S_NEW => 'New', self::S_ACTIVE => 'Active', self::S_INACTIVE => 'Inactive');
	protected $_level = array(self::L_ADMIN => 'Admin', self::L_EDITOR => 'Editor', self::L_MODERATOR => 'Moderator',
		self::L_USER => 'User');

	public function login($remember = '') {
		if ($this->id_status == self::S_ACTIVE) {
			return true;
		} else {
			return false;
		}
	}

	public function get_status($id = null) {
		if ($id === null) {
			return $this->_status;
		}
		if (isset($this->_status[$id])) {
			return $this->_status[$id];
		}
		return '';
	}

	public function get_level($id = null) {
		if ($id === null) {
			return $this->_level;
		}
		if (isset($this->_level[$id])) {
			return $this->_level[$id];
		}
		return '';
	}

	public function get_datatable_list() {
		$data = array();
		$users = $this->load(\DB::select_array(), NULL);

		foreach ($users as $row) {
			$row['level'] = $this->_level[$row['id_level']];
			$row['status'] = $this->_status[$row['id_status']];
			$row['action'] = \html::anchor('sysusers/edit/' . $row['id'], 'Edit');
			$data[] = $row;
		}
		return $data;
	}

	public function save($validation = null) {
		if (empty($this->_data['id'])) {
			$this->date_created = date('Y-m-d H:i:s');
		}
		$this->email = strtolower($this->email);

		// Don't save the password
		if (($this->passwrd == '') || (strlen($this->passwrd) == 32)) {
			unset($this->_data['passwrd']);
		} else {
			$this->passwrd = \Authlite::instance('auth_sys')->hash($this->passwrd);

		}
		parent::save();
	}

	public function notify_comm() {
		$data = array();
		$stmt = $this->db->rawquery("SELECT email FROM {$this->_table} WHERE notify_comm = 1 AND id_status = 1");
		$stmt->execute();
		$result = $stmt->fetchAll();

		foreach ($result as $row) {
			$data[] = $row['email'];
		}
		return $data;
	}

}