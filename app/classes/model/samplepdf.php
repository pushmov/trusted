<?php
namespace Model;

class Samplepdf extends \Model {
	protected $_template;
	protected $_page;
	protected $_preview;

	public function load($id, $page = null, $preview = false) {
		$this->_template = \Model\Template::forge(array('id' => $id));
		$this->_page = $page;
		$this->_preview = $preview;
		return $this;
	}

	public function gen_pdf($as_email = false) {
		\Package::load('tcpdf');
		$pdf = new \TCPDF('P', 'mm', 'Letter');
		// Set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor("Trusted and Referred");
		$pdf->SetTitle("Trusted and Referred");
		$pdf->SetSubject("Trusted and Referred");

		// Remove default header/footer
		$pdf->setPrintHeader(FALSE);
		$pdf->setPrintFooter(FALSE);
		$pdf->SetMargins(0, 0, -1);
		$pdf->SetAutoPageBreak(FALSE, 0);

		$data = \Model\Field::forge()->get_pdf_data($this->_template->id, $this->_page);


		if ($data->count()) {
			$page = ($this->_page === null) ? 1 : $this->_page;
			foreach($data as


		} else {
			// Log the error
		}






		require_once 'tcpdf/config/tcpdf_config.php';
		require_once 'tcpdf/tcpdf.php';

		$result = $this->_db->rawquery("SELECT * FROM pdf_templates WHERE id = " . $this->_template);
		$template = $result->fetch();
		$result = $this->_db->rawquery("SELECT * FROM pdf_fields");

		while ($row = $result->fetch()) {
			$fields[$row['id']] = $row;
		}
		$this->_data['realtor'] = $this->_data['first_name'] . ' ' . $this->_data['last_name'];

		// Placeholders
		foreach ($this->_data as $k => $v) {
			$placeholders['{{' . $k . '}}'] = $v;
		}

		$pdf = new TCPDF('P', 'mm', 'Letter');

		if ($this->_page != 0) {
			$sp = $this->_page;
			$ep = $this->_page;
		} else {
			$sp = 1;
			$ep = $template['pages'];
		}

		// Determine the house image
		foreach (array('png', 'jpg', 'jpeg') as $ext) {
			$fileName =
				SRV_PATH . 'testdrive/pdf_images/test_' . str_pad($this->_data['id'], 3, '0', STR_PAD_LEFT) . '.'
				. $ext;

			if (is_readable($fileName)) {
				$photo_h = $fileName;
				break;
			}
		}

		if (!isset($photo_h)) {
			$photo_h = SRV_PATH . 'testdrive/pdf_images/' . str_pad($this->_template, 3, '0', STR_PAD_LEFT) . '_photo_h.jpg';
		}

		for ($page = $sp; $page <= $ep; $page++) {
			$pdf->AddPage();
			// Set bacKground image
			$img_file = SRV_PATH . 'testdrive/pdf_images/' . str_pad($template['id'], 3, '0', STR_PAD_LEFT) . '_'
				. str_pad($page, 3, '0', STR_PAD_LEFT) . '.jpg';
			$pdf->Image($img_file, 0, 0, 215.9, 279.4, '', '', '', FALSE, 150, '', FALSE, FALSE, 0);
			$result = $this->_db->rawquery(
				"SELECT * FROM pdf_field_pos WHERE id_template = {$template['id']} AND page = {$page}");

			while ($row = $result->fetch()) {
				if ($fields[$row['id_field']]['id_status'] == 3) {
					// only image now is the house
					if ($row['id_field'] == 11) {
						if (is_readable($photo_h)) {
							if ($row['width'] > 0) {
								$width = $row['width'];
								$height = 0;
							} else {
								$width = 0;
								$height = $row['height'];
							}
							$pdf->Image($photo_h, $row['pos_x'], $row['pos_y'], $width, $height, '', '', '', FALSE, 150,
								'', FALSE, FALSE, 0);
						}
					}
				} else {
					$pdf->SetTextColorArray(explode(',', ($row['font_color'] == '') ? '0,0,0' : $row['font_color']));
					$pdf->SetFont('times', $row['font_style'], $row['font_size']);
					$pdf->SetXY($row['pos_x'], $row['pos_y']);

					if ($row['content'] == '') {
						$pdf->Cell($row['width'], 0, $this->_data[$fields[$row['id_field']]['field']], 0, 0,
							$row['text_align']);
					} else {
						$str = strtr($row['content'], $placeholders);
						$pdf->Cell($row['width'], 0, $str, 0, 0, $row['text_align']);
					}
				}
			}
		}
		// Reset pointer to the last page
		$pdf->lastPage();

		// Close and output PDF document
		if ($as_email) {
			return $pdf->Output('testdrive.pdf', 'S');
		} else {
			$pdf->Output('testdrive.pdf', 'I');
		}
	/*

	$pdf =


	$template =

		$myPageWidth = $pdf->getPageWidth();
$myPageHeight = $pdf->getPageHeight();
$myX = ( $myPageWidth / 2 ) - 50;
$myY = ( $myPageHeight / 2 ) -40;

$pdf->SetAlpha(0.09);
$pdf->Image(K_PATH_IMAGES.'SACS.png', $myX, $myY, $ImageW, $ImageH, '', '', '', true, 150);
		   */
	}
}