<?php
namespace Model;

/**
* Stores the user's client information
*/
class Client extends \Model_Crud {
	const S_NEW = 0; // New not validated
	const S_ACTIVE = 1;
	const S_PASSWORD = 2; // Needs new password
	const S_HOLD = 7; // Admin put on hold for any reason
	const S_ARCHIVE = 9; // Account closed, info archived
	
	const IMG_DIR = 'assets/img/';
	const IMG_PATH = 'client/';

	protected static $_table_name = 'clients';
	protected static $_mysql_timestamp = true;
	protected static $_created_at = 'created_at';

	protected static $_defaults = array('id' => null, 'full_names' => '', 'single_names' => '', 'address' => '', 'city' => '',
		'state' => '', 'zip' => '', 'email' => '', 'user_id' => 0, 'created_at' => '', 'status_id' => 0);
	protected static $_mass_whitelist = array('id', 'full_names', 'single_names', 'address', 'city', 'state', 'zip', 'email', 'user_id', 'created_at');
	protected static $_rules = array('full_names' => 'required', 'single_names' => 'required', 'address' => 'required', 'city' => 'required',
		'state' => 'required', 'zip' => 'required', 'email' => 'required|valid_email');
	protected static $_labels = array('full_names' => 'Full Name', 'single_names' => 'Single Name', 'address' => 'Address',
		'city' => 'City', 'state' => 'State', 'zip' => 'ZIP Code', 'email' => 'Email');
	protected $_status = array(0 => 'New', 'Active', self::S_HOLD => 'Hold', self::S_ARCHIVE => 'Closed');
	// These fields can be used in the pdf
	protected static $_pdf_fields = array('full_names' => 'Client Full Names', 'single_names' => 'Client Single Names',
		'address' => 'Client Address', 'city' => 'Client City',	'state' => 'Client State', 'zip' => 'Client Zip', 'email' => 'Client Email');

	public function get_fields() {
		return self::$_pdf_fields;
	}

	public function get_status_array() {
		return $this->_status;
	}

	protected function post_save($result) {
		//rename photo if any
		$file = \Model\Client::IMG_DIR . \Model\Client::IMG_PATH . \Session::get('property_photo');
		if(\Session::get('property_photo') && !empty($file) && is_readable(DOCROOT.$file)) {
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$filename = str_pad($this->id, 8, '0', STR_PAD_LEFT).'.'.$ext;
			rename($file , \Model\Client::IMG_DIR . \Model\Client::IMG_PATH . $filename);
			\Session::delete('property_photo');
		}
		return $result;
	}

	public function all_clients($params) {
		$data['data'] = array();
		$data['draw'] = (isset($params['draw'])) ? (int)$params['draw'] : '1';
		$aColumns = array('full_names', 'address', 'city');
		if($params['hasuser'] === true) {
			$aColumns[] = 'assoc';
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "
				. "(SELECT clients.id,clients.full_names,clients.single_names,clients.address,clients.city,clients.state,clients.zip,clients.email,clients.user_id,CONCAT(users.first_name,' ',users.last_name) AS assoc,users.id AS associd "
				. "FROM ".self::$_table_name." LEFT JOIN users ON clients.user_id = users.id WHERE users.id IN ('{$params['users']}')) AS tmp";
		} else {
			$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM "
				. "(SELECT clients.id,clients.full_names,clients.single_names,clients.address,clients.city,clients.state,clients.zip,clients.email,clients.user_id, users.id AS associd FROM ".self::$_table_name." INNER JOIN users ON users.id = clients.user_id) AS tmp"
				. " WHERE user_id = '{$params['user_id']}'";
		}
		$result = \DB::query($sql, \DB::SELECT)->execute();	
		$data['recordsTotal'] = \DB::query($sql, \DB::SELECT)->execute()->count();
		if (isset($params['search']['value']) && $params['search']['value'] != ''){
			$sql .= " AND (";
			$sql .= "full_names LIKE '%{$params['search']['value']}%'";
			$sql .= " OR single_names LIKE '%{$params['search']['value']}%'";
			$sql .= " OR address LIKE '%{$params['search']['value']}%'";
			$sql .= " OR city LIKE '%{$params['search']['value']}%'";
			$sql .= " OR state LIKE '%{$params['search']['value']}%'";
			$sql .= " OR zip LIKE '%{$params['search']['value']}%'";
			$sql .= " OR email LIKE '%{$params['search']['value']}%'";
			if($params['hasuser'] === true) {
				$sql .= " OR assoc LIKE '%{$params['search']['value']}%'";
			}
			$sql .= ")";
		}
		if (isset($params['order']) && ((int)$params['order'] > 0)) {
			$sql .= " ORDER BY {$aColumns[$params['order'][0]['column']]} {$params['order'][0]['dir']}";
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($params['start']) && $params['length'] != '-1') {
			$sql .= " LIMIT {$params['start']}, {$params['length']}";
		}
		$aColumns[] = 'action';
		foreach ($result as $row) {
			$row['action']  = \Form::button('button', '<i class="fa fa-eye"></i>Profile', array('class' => 'button small client-profile', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-trash"></i>Remove', array('class' => 'button small alert client-remove', 'type' => 'button', 'data-id' => $row['id']));
			$row['action']  .= \Form::button('button', '<i class="fa fa-envelope"></i>Email', array('class' => 'button small client-email', 'type' => 'button', 'data-id' => $row['id']));
			$data['data'][] = array_values(\Arr::filter_keys($row, $aColumns));
		}
		$data['recordsFiltered'] = \DB::query('SELECT FOUND_ROWS() AS cnt', \DB::SELECT)->execute()->get('cnt');
		return $data;
	}

	public function send_message($data){
		$response = array();
		$val = \Validation::instance();
		$val->add_field('email', 'Email', 'required|valid_email');
		$val->add_field('name', 'Name', 'required');
		$val->add_field('subject', 'Subject', 'required');
		$val->add_field('message', 'Message', 'required');
		if (!$val->run($data)) {
			$response['error']['message'] = $val->error_message();
			return $response;
		}
		return \Emails::forge()->client_send_message($data);
	}

	public function remove($id){
		$file = \Files::forge()->load(\Model\Client::IMG_DIR . \Model\Client::IMG_PATH . str_pad($id, 8, '0', STR_PAD_LEFT));
		if($file) {
			unlink(DOCROOT.$file);
		}
		$client = self::find_by_pk($id);
		if ($client) {
			$client->delete();
		}
		return true;
	}
}
