<?php
namespace Controller\Users;

class Team extends Auth {
	public function action_profile(){
		$view = \View::forge('user/team_profile');
		if(!$this->param('id')) {
			\Response::redirect('users/dashboard');
		}
		$data = \Model\User::find_by_pk($this->param('id'))->to_array();
		$data['phone'] = $data['website'] = $data['cloud_link'] = '';
		if (empty($this->group)) {
			\Response::redirect('users/dashboard');
		}
		if ($this->user->id == $this->param('id')) {
			\Response::redirect('users/profile');
		}
		$data['full_name'] = $data['first_name'].' '.$data['last_name'];
		//team leader only
		if (\Model\Team::find_one_by_agent_id($this->user->id) === null) {
			\Response::redirect('users/dashboard');
		}
		$view->data = $data;
		$this->template->content = $view;
		$this->template->meta_title = 'Trusted - Team Profile';
		$this->template->page_title = 'Profile';
		$this->template->scripts[] = 'user.js';
	}
	
	public function get_add_user(){
		return $this->response(\View::forge('dialog/form_add_user'));
	}

	public function post_add_user(){
		$data = \Input::post('data');
		$data['user'] = (array) $this->user;
		return $this->response(\Model\Team::forge()->invite($data));
	}
	
	public function post_update(){
		$response = array();
		$user = \Model\User::find_by_pk(\Input::post('data.id'));
		$validated = true;
		$validation = \Validation::instance();
		$validation->add_callable('rules');
		$validation->add_field('email', 'Email', 'unique_email_self['.$user->id.']');
		if (!$validation->run(\Input::post('data'))) {
			$validated = false;
			$response['errors']['message'] = $validation->error_message();
		}
		$user->set(\Input::post('data'));
		$vals = $user->validation();
		$vals->add_callable('rules');
		if ( !$user->validates() ) {	
			$validated = false;
			$response['errors']['message'] = array_merge($vals->error_message(), $validation->error_message());
		}
		if (!$validated) {
			return $this->response($response);
		}
		$user->save();
		$response['success']['message'] = 'User profile updated';
		return $this->response($response);
	}
	
	public function get_confirm_delete(){
		$view = \View::forge('dialog/user/confirm_delete');
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_remove(){
		return $this->response(\Model\User::forge()->remove(\Input::post('id')));
	}
	
	public function get_load_team_table(){
		return $this->response(\View::forge('user/my_team'));
	}
}