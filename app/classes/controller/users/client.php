<?php

namespace Controller\Users;

class Client extends Auth {
	public function action_profile() {
		$view = \View::forge('client/profile');
		$view->data = \Model\Client::find_by_pk($this->param('id'))->to_array();
		if ($this->user->id != $view->data['user_id']) {
			\Response::redirect('users/dashboard');
		}
		$this->template->content = $view;
		$this->template->meta_title = 'Trusted - Client Profile';
		$this->template->page_title = 'Profile';
		$this->template->js_vars = 'var client_id = '.$view->data['id'].';' . PHP_EOL;
		$this->template->scripts = array_merge(array('jquery.uploadfile.min.js', 'client.js'), $this->template->scripts);
	}
	
	public function post_update(){
		$response = array();
		$client = \Model\Client::find_by_pk(\Input::post('data.id'));
		$client->set(\Input::post('data'));
		if ( $client->validates() ) {
			$client->save();
			$response['success']['message'] = 'Client data successfully updated';
		} else {
			$response['error']['message'] = $client->validation()->error_message();
		}
		return $this->response($response);
	}
	
	public function get_add_client(){
		$view = \View::forge('dialog/form_add_client');
		$data['country'] = 'CA';
		$view->data = $data;
		return $this->response($view);
	}

	public function post_add_client(){
		$response = array();
		$data = \Input::post('data');
		$data['user_id'] = $this->user->id;
		$client = \Model\Client::forge($data);
		if(!$client->save()) {
			$response['error']['message'] = $client->validation()->error_message();
		} else {
			$response['success']['message'] = 'Client data successfully saved';
		}
		return $this->response($response);
	}
	
	public function post_doupload(){
		$output_dir = DOCROOT . \Model\Client::IMG_DIR . \Model\Client::IMG_PATH;
		$json = array();
		if (!is_dir($output_dir)){
			mkdir($output_dir);
		}
		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$name = \Input::post('id') ? \Input::post('id') : $this->user->id;
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$filename = str_pad($name, 8, '0', STR_PAD_LEFT).'.'.$ext;
			if(is_readable(DOCROOT. \Model\Client::IMG_PATH.$filename)){
				unlink(DOCROOT. \Model\Client::IMG_PATH.$filename);
			}
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $filename);
			$json[] = $filename;
			\Session::set('property_photo', $filename);
		}
		return $this->response($json);
	}

	public function get_lists() {
		$param = \Input::get();
		$param['user_id'] = $this->user->id;
		$user = \Model\User::find_by_pk($this->user->id);
		$has_user = \Model\User::find_by_parent_id($this->user->id);
		$param['hasuser'] = ($user->parent_id == '0' && count($has_user) > 0) ? true : false;
		$users = \Model\User::forge()->get_team_member($this->user->id);
		$param['users'] = join("','",$users);
		return $this->response(\Model\Client::forge()->all_clients($param));
	}

	public function get_confirm_delete() {
		$view = \View::forge('dialog/client/confirm_delete');
		$view->id = \Input::get('id');
		return $this->response($view);
	}

	public function post_remove() {
		return $this->response(\Model\Client::forge()->remove(\Input::post('id')));
	}
	
	public function get_send_message() {
		$view = \View::forge('dialog/form_email_client');
		$view->data = \Model\Client::find_by_pk(\Input::get('id'))->to_array();
		return $this->response($view);
	}
	
	public function post_send_message() {
		return $this->response(\Model\Client::forge()->send_message(\Input::post('data')));
	}
	
	public function get_profile_picture() {
		$file = \File::load(\Model\Client::IMG_DIR . \Model\Client::IMG_PATH . str_pad(\Input::get('id'), 8, '0', STR_PAD_LEFT));
		if($file === false) {
			return;
		} else {
			$logo = \Model\Client::IMG_PATH . basename($file);
			return $this->response(\Asset::img($logo, array('width' => 480)));
		}
	}

}