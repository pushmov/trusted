<?php

namespace Controller\Users;

class Auth extends \Controller_Hybrid {
	public $user;
	public $template = 'user_template';

	public function before(){
		parent::before();
		if (!\Authlite::instance('auth_user')->logged_in()) {
			\Response::redirect('/login');
		}
		$this->auto_render = !\Input::is_ajax();
		if ($this->auto_render) {
			// Initialize empty values
			$this->template->meta_title = '';
			$this->template->meta_desc = '';
			$this->template->title = '';
			$this->template->content = '';
			$this->template->page_title = '';

			$this->template->styles = array();
			$this->template->scripts = array();
		}
		$this->user = \Authlite::instance('auth_user')->get_user();
	}

	public function after($response) {
		return parent::after($response);
	}
}