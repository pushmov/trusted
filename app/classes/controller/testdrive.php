<?php
namespace Controller;

class Testdrive extends PublicTemplate {

	public function action_index() {
		$view = \View::forge('form_page');
		$view->data = \Model\Testdrive::forge()->to_array();
		$this->template->meta_title = 'Test Drive | Welcome';
		$this->template->content = $view;
		$this->template->scripts = array('jquery.uploadfile.min.js', 'upload.js');
	}

	public function post_send() {
		try {
			\Model\Testdrive::forge()->send(\Input::post());
			$json['status'] = 'OK';
			$json['message'] = '<div class="callout success">Your presentation has been sent. Check your inbox!</div>';
			return $this->response($json);
		} catch(\AutoModelerException $e) {
			return $this->response(array('errors' => $e->errors));
		}
	}

	public function action_preview($uuid = null) {
		if (!empty($uuid)) {
			$pres = \Model\Testdrive::forge()->load_by_uuid($uuid);
			if ($pres->loaded()) {
				$pres->gen_pdf();
				exit;
			}
		}
	}

	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Testdrive::PDF_PATH;
		$json = array();

		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$fileName = \Session::key() . '.' . strtolower($ext);

			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $fileName);
			$json[] = $fileName;
		}
		return $this->response($json);
	}

	/**
	* The 404 action for the application.
	*
	* @access  public
	* @return  Response
	*/
	public function action_404() {
		return \Response::forge(\View::forge('404'), 404);
	}

	public function action_sample() {
		$pdf = new \Model\Samplepdf;
		$pdf->load(3, 51, true)->gen_pdf();
	}
}
