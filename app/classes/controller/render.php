<?php
namespace Controller;

class Render extends \Controller {
	public function action_index() {
	}

	public function action_pdf() {
		$token = $this->param('token');
		if (!empty($token)) {
			$pres = \Model\Template\Pdf::forge()->find_by_pk($token);
			if ($pres->id !== null) {
				$header = array('Content-Type' => 'application/pdf');
				\Response::forge($pres->render(($this->param('page')) ? $this->param('page') : null), 200, $header);
				exit;
			}
		}
	}
	public function action_jpg() {
		$token = $this->param('token');
		if (!empty($token)) {
		}
	}
}
