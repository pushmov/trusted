<?php
namespace Controller;

class Signup extends PublicTemplate {

	public function action_index() {
		$content = \View::forge('signup');
		$this->template->scripts = array('signup.js');
		$this->template->meta_title = 'Registration';
		$this->template->content = $content;
	}

	public function get_state() {
		return $this->response(\StateInfo::get_state_form(\Input::get('country')));
	}

	public function post_submit() {
		$response = array();
		$validated = true;
		$data = \Input::post('data');
		$user = \Model\User::forge($data['user']);
		$uval = $user->validation();
		$uval->add_callable('rules');
		if (!$user->validates()) {
			$response['error']['message']['user'] = $uval->error_message();
			$validated = false;
		}

		$validation = \Validation::instance();
		$validation->add_callable('rules');
		$validation->add_field('email', 'Email', 'unique_email[user]');
		$validation->add_field('passwrd', 'Password', 'required|min_length[6]');
		$validation->add_field('cpasswrd', 'Confirm Password', 'required|match_field[passwrd]');
		if (!$validation->run($data['user'])) {
			$validated = false;
			$response['error']['message']['user'] = array_merge($uval->error_message(), $validation->error_message());
		}

		$user->status_id = \Model\User::S_ACTIVE;
		$user->token = \Model\User::forge()->generate_token($user->email);
		$user->save();

		$team->broker_id = 0; //todo : broker ?
		$team->status_id = \Model\Team::S_ACTIVE;
		$team->agent_id = $user->id;
		$team->save();

		$response['success'] = array('message' => 'Your account successfully registered. Please check your inbox to activate your account.');
		$data['user']['token'] = $user->token;
		\Emails::forge()->welcome($data);
		return $this->response($response);
	}
}