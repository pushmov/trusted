<?php
namespace Controller\Admin;

class Login extends \Controller_Template {
	public $template = 'admin/admin_base';
	protected $_authlite;

	public function before() {
		parent::before();
		$this->_authlite = \Authlite::instance('auth_sys');
		$this->_authlite->logout();
	}
	public function action_index() {
		if (\Input::post('login')) {
			$data = \Input::post();
			$pw = $this->_authlite->hash(\Input::post('passwrd'));
			if ($this->_authlite->login(\Input::post('email'), \Input::post('passwrd'))) {
				$user = $this->_authlite->get_user();
				if (\Model\Sysuser::forge($user->id)->login()) {
					\Response::redirect('/admin/dashboard');
				}
			}
			$data['message'] = '<div class="callout alert small">Login Error. Try again</div>';
		} else {
			$data = array('email' => '', 'passwrd' => '', 'message' => '');
		}
		$this->template->content = \View::forge('admin/login', $data, false);
	}

	public function action_logout() {
		\Response::redirect('/admin/login');
	}
}