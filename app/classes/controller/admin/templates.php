<?php
namespace Controller\Admin;

class Templates extends \Controller_Rest {
	public function post_save() {
		\Model\Coord::forge()->save_post(\Input::post());
		$id=\Input::post('id_template');
		$page=\Input::post('page');
		$status='OK';
		$this->get_dopreview($id,$page,$status);
		return $this->response(array('status' => 'OK'));
	}

	public function post_preview() {
		\Model\Coord::forge()->save_post(\Input::post());
		$json = array('url' => \Uri::create('admin/templates/dopreview/' . \Input::post('id_template') . '/' . \Input::post('page')));
		return  $this->response($json);
	}

	public function get_dopreview($id,$page,$status='') {
		$pres = \Model\Testdrive::forge(1);
		if ($pres->loaded()) {
			$pres->gen_pdf($id,$page,FALSE,$status);
			exit;
		}
	}
   public function post_saveImage(){           		   
			    if (isset($GLOBALS["HTTP_RAW_POST_DATA"]))
			{
			  // Get the data
			  $imageData=$GLOBALS['HTTP_RAW_POST_DATA'];
		      // Remove the headers (data:,) part.
			  // A real application should use them according to needs such as to check image type
			  $filteredData=substr($imageData, strpos($imageData, ",")+1);
			  $filterData= explode('-',$filteredData);
			 
			  // Need to decode before saving since the data we received is already base64 encoded
			  $unencodedData=base64_decode($filterData[0]);			 
			   
			  // Save file. This example uses a hard coded filename for testing,
			  // but a real application can specify filename in POST variable
			  $fp = fopen( 'assets/imagePdf/'.$filterData[1].'.png', 'wb' );
			  fwrite( $fp, $unencodedData);
			  fclose( $fp );
			}
   }

	public function post_doupload() {
		$output_dir = DOCROOT . \Model\Testdrive::PDF_PATH;
		$json = array();

		if ($_FILES["myfile"]["error"]) {
			$json['msg'] = $_FILES["myfile"]["error"];
		} else {
			$ext = pathinfo($_FILES["myfile"]["name"], PATHINFO_EXTENSION);
			$fileName = str_pad(\Input::post('template'), 3, '0', STR_PAD_LEFT) . '_' . str_pad(\Input::post('page'), 3, '0', STR_PAD_LEFT) . '.' . strtolower($ext);
			move_uploaded_file($_FILES["myfile"]["tmp_name"], $output_dir . $fileName);
			$json[] = $fileName;
		}
		return $this->response($json);
	}

}