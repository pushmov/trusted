<?php
namespace Controller\Admin;

class Dashboard extends Auth {
	public function action_index() {
		$page = \View::forge('admin/dashboard', null, false);
		/*echo "<pre>";
		print_r($page);
		echo "</pre>"; die;*/
		if (\Input::post('id_template')) {
			$post = \Input::post();
		} else {
			$post = array('id_template' => 1, 'page' => 1);
		}
		$template = \Model\Template::forge($post['id_template']);
		// New page
		if (\Input::post('page', 'new') == 'new') {
			$template->pages++;
			$post['page'] = $template->pages;
			$template->save();
		}
		if ($post['page'] > $template->pages) {
			$post['page'] = $template->pages;
		}

		$pages = range(1, $template->pages);
		$page->cbo_templates = $template->select_list('id', 'title');
		$page->id_templates = $template->select_list('id', 'id');
		$page->cbo_pages = array_combine($pages, $pages);
		$page->id_template = $post['id_template'];
		$page->page = $post['page'];
		$page->fields = \Model\Coord::forge()->get_datatable_list($post);
        
		$this->template->set('styles', \Asset::css('uploadfile.css'), false);		
		$this->template->set('scripts', \Asset::js('jquery.uploadfile.min.js'), false);


		$this->template->content = $page;
	}
}