<?php

namespace Controller;

class Profile extends PublicTemplate {
	public function get_profile_picture(){
		$file = \Files::forge()->load('assets/images/'.str_pad(\Input::get('id'), 8, '0', STR_PAD_LEFT));
		if($file === false){
			$logo = 'no-image.png';
		} else {
			$logo = 'vendors/'.basename($file);
		}
	}

}