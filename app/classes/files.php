<?php
class Files {
	
	public static function forge() {
		return new self;
	}
	
	public function load($path){
		$file = glob($path . '.*');
		usort($file, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));
		if(!empty($file) && is_readable(DOCROOT.current($file))) {
			return current($file);
		}
		return false;
	}
	
}