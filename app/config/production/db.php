<?php
/**
 * The production database settings. These get merged with the global settings.
 */
return array(
	'default' => array(
		'type'           => 'mysqli',
		'connection'     => array(
			'hostname'       => 'localhost',
			'port'           => '3306',
			'database'       => 'trustedref',
			/*'username'       => 'trusteda_app',
			'password'       => 'YOlH8q7PbzdNBoPz',*/
			'username'       => 'root',
			'password'       => 'root1',
			'persistent'     => false,
			'compress'       => false,
		),
		'identifier'     => '`',
		'table_prefix'   => '',
		'charset'        => 'utf8',
		'enable_cache'   => false,
		'profiling'      => false,
		'readonly'       => false,
	),
);