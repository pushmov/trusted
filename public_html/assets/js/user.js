$(document).ready(function(){
   $('#btn_user_update').on('click', function(){
       var form = $(this).closest('form');
       form.find('input').removeClass('validation-error');
       form.find('.validation-input-error').remove();
       form.find('.highlight-alert').remove();
       $.post(form.attr('action'), form.serialize(), function(json){
           if(json.errors) {
                $.each(json.errors.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
           } else if(json.success) {
               
               form.find('#btn_user_update').before('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
           }
       });
   }); 
});