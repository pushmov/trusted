var C = 11.811;
var w = $('#width').val() * C;
var h = $('#height').val() * C;
var paper = Raphael(document.getElementById('panel'), w, h);
var freeTransformConfig = { 
    drag: 'self',
    scale: true,
    rotate: false,
    draw: false,
    attrs: {
        fill: '#ff0000',
        stroke: '#000'
    },
    size: 25
};
paper.setViewBox(0,0,w,h,true);
paper.setSize('100%', '100%');

var rect = null, ft = null;
function setPageImage(){
    $.get('/users/template/image.json', {template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        paper.clear();
        if(json.error) {
            paper.image('', 0, 0, '100%', '100%');
        } else if(json.success) {
            var ratio = 0;
            var width = parseInt(json.attrs.width);
            var height = parseInt(json.attrs.height);
            
            if(width > w) {
                ratio = w / width;
                paper.image('/' + json.image, 0, 0, w, height * ratio);
                height = height * ratio;
                width = width * ratio;
            } else {
                paper.image('/' + json.image, 0, 0, width, height);
            }
            
            if(height > h) {
                ratio = h / height;
                paper.image('/' + json.image, 0, 0, width * ratio, h);
                width = width * ratio;
                height = height * ratio;
            } else {
                paper.image('/' + json.image, 0, 0, width, height);
            }
        }
    });
}

function setObject(id){
    $.get('/users/template/placement.json', {id: id, template_id: template_id, page_id: $('#page_dropdown').val()}, function(json){
        if (json.error) {
            return false;
        } else {
            var x = parseFloat(json.pos_x) * C;
            var y = parseFloat(json.pos_y) * C;
            var w = parseFloat(json.width) * C;
            var h = parseFloat(json.height) * C;
            if(json.shape_id === '0') {
                rect = paper.rect(x, y, w, h)
                    .attr('fill', 'rgba(0, 0, 255, 0.28)');
                freeTransformConfig.keepRatio = false;
            } else if(json.shape_id === '1') {
                var r = h / 2;
                
                rect = paper.circle((x + r),(y + r),r)
                    .attr('fill', 'rgba(0, 0, 255, 0.28)');
                freeTransformConfig.keepRatio = true;
            }
            ft = paper.freeTransform(rect);
            ft.hideHandles();
            ft.showHandles();
            ft.attrs.rotate = 0;
            ft.apply();
            ft.unplug();
            ft = paper.freeTransform(rect, freeTransformConfig, function(ft, events) {
    //            console.log(ft.attrs);
            });
            rect.toFront();
        }
    });
}

$('#save_placement').on('click', function(){
    $('.page-image-control').parent().find('.alert-box').remove();
    var x,y,w,h;
    if(rect.node.tagName === 'circle') {
        var pos = rect.getBBox();
        var r = pos.width / 2;
        w = (r * 2) / C;
        h = (r * 2) / C;
        x = (pos.cx - r) / C;
        y = (pos.cy - r) / C;
    } else if(rect.node.tagName === 'rect') {
        var bBox = rect.getBBox();
        w = bBox.width / C;
        h = bBox.height / C;
        x = bBox.x / C;
        y = bBox.y / C;
    }
    var data = {
        pos_x: RoundNum(x, 1),
        pos_y: RoundNum(y, 1),
        width: RoundNum(w, 1),
        height: RoundNum(h, 1),
        template_id : template_id,
        page_id : $('#page_dropdown').val(),
        id: $(this).attr('data-id'),
//        rotate: rect.matrix.split().rotate
        rotate: 0
    };
    
    var btn = $(this);
    var txt = btn.html();
    btn.prop('disabled', true);
    btn.html(loader);
    $.post('/users/template/placement.json', data, function(json){
        btn.prop('disabled', false);
        btn.html(txt);
        if(json.error) {
            var m = '<div data-alert class="alert-box danger">';
            m += '<ul class="unstyled">';
            c = 'danger';
            
            if(json.error.message.pos_x && json.error.message.pos_y) {
                json.error.message.pos_x = 'Minimum x and y values are 5.0';
                delete json.error.message.pos_y;
            }
            if(json.error.message.width && json.error.message.height) {
                delete json.error.message.width;
            }
            $.each(json.error.message, function(k, v){
                m += '<li>' + v + '</li>';
            });
            m += '</ul>';
            m += '</div>';
        }
        else if (json.success) {
            var m = '<div data-alert class="alert-box success">';
            m += '<ul class="unstyled">';
            m += '<li>'+json.success.message+'</li>';
            m += '</ul>';
            m += '</div>';
        }
        if (ft !== null) {
            rect.remove();
            ft.unplug();
        }
        setObject(json.id);
        dtf.ajax.reload();
        setTimeout(function(){
            $(document).find('tr').removeClass('row-selected');
            $(document).find('.placement-value[data-id="'+json.id+'"]').closest('tr').addClass('row-selected');
        }, 1500);
        $('#save_placement').parent().after(m);
    });
});

$(document).on('click', '.placement-value', function(){
    if(ft !== null) {
        rect.remove();
        ft.unplug();
    }
    setObject($(this).attr('data-id'));
    $('#save_placement').attr('data-id',$(this).attr('data-id'));
});

$('#page_dropdown').on('change', function(){
    if(ft !== null) {
        rect.remove();
        ft.unplug();
    }
    setPageImage();
    setObject(null);
    dtf.ajax.reload();
    $.get('/users/template/upload_page.html', {page: $(this).val(), template: template_id}, function(html){
        $('#upload_page_image_btn').html(html);
    });
});