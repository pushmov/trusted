$(document).ready(function(){
    
    $('#btn_signup').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);

        var form = $(this).closest('form');
        form.find('.validation-error').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        form.find('.highlight-alert').remove();
        
        $.post(form.attr('action'), form.serialize(), function(data){
            btn.html(txt);
            btn.prop('disabled', false);
            if(data.error){
                $.each(data.error.message, function(i, j){
                    $.each(j, function(k,v){
                        var elem = $(document).find('#' + i + '_' + k );
                        if (k === 'state') {
                            elem = $(document).find('#' + k);
                        }
                        elem.addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                    });
                });
            } else if(data.success) {
                form.find('input, select').val('');
                $('#btn_signup').before('<div class="highlight-alert"><a href="#" class="highlight tos">'+data.success.message+'</a></div>');
            }
        });
    });

    $('#user_email').on('blur', function(){
        $(this).removeClass('validation-error');
        $(this).parent().find('.validation-input-error').remove();
        $.post('/users/email_exists.json', {email: $(this).val()}, function(json){
            if(json.error) {
                $('#user_email').addClass('validation-error').after('<div class="validation-input-error">'+json.error.message+'</div>');
            }
        });
    });

    $('#user_email').on('keyup', function(){
        $(this).removeClass('validation-error');
        $(this).parent().find('.validation-input-error').remove();
    });
    
    $('.show-password').hover(function(){
        $(this).parent().parent().find('input[type="password"]').attr('type', 'text');
    }, function(){
        $(this).parent().parent().find('input[type="text"]').attr('type', 'password');
    });
    
});