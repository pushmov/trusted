$(document).ready(function(){
    $.get('/users/client/profile_picture.html', {id: client_id}, function(html){
        $('#client_property').html(html);
    });

    $('#file_upload').uploadFile({
		url: '/users/client/doupload.json',
		dragDrop: false,
		fileName: 'myfile',
		allowedTypes: 'jpg,jpeg,png',
		returnType: 'json',
        uploadStr:'Change Property Photo',
        formData: {'id' : client_id},
		onSuccess: function(files,data,xhr) {
			$('#img_file').val(data[0]);
            $.get('/users/client/profile_picture.html', {id: client_id}, function(html){
                $('#client_property').html(html);
            });
		}
	});

    $('#btn_client_update').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.html(loader);
        btn.prop('disabled', true);
        var form = $(this).closest('form');
        form.find('input').removeClass('validation-error');
        form.find('.validation-input-error').remove();
        form.find('.highlight-alert').remove();
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.html(txt);
            btn.prop('disabled', false);
            if (json.error) {
                $.each(json.error.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
            } else if(json.success){
                form.find('#btn_client_update').before('<div class="highlight-alert"><a href="#" class="highlight success">'+json.success.message+'</a></div>');
            }
        });
    });

});