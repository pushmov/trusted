var dgf;
$(document).ready(function(){
    
    $('#btn_save_group').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        var form = $(this).closest('form');
        $.post(form.attr('action'), form.serialize(), function(json){
            btn.prop('disabled', false);
            btn.html(txt);
            if(json.error) {
                $.each(json.error.message, function(k,v){
                    $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                });
            } else if(json.success) {
                $('#notification').html(json.success.notification);
            }
        });
    });
    
    if ($('#tbl_group_fields').length > 0) {
        dgf = $('#tbl_group_fields').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            ordering: false,
            ajax: {
                url: '/users/group/fields.json',
                data: function ( d ) {
                    d.group_id = group_id;
                }
            },
            responsive: true,
            iDisplayLength: 25
        });
        $(document).on({
            mouseenter: function(){
                $(this).addClass('row-highlighted');
            },
            mouseleave: function(){
                $(this).removeClass('row-highlighted');
            }
        }, 'tr');

    }
    
    $(document).on('click', '.btn-group-field-delete', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/users/group/field_delete.html', {id: $(this).attr('data-id')}, function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog').html(html);
            $('#dialog').foundation('open');
            $('#dialog').on('click', '#btn_delete_group_field', function(){
                var btn = $(this);
                var txt = btn.html();
                var form = $(this).closest('form');
                btn.prop('disabled', true);
                btn.html(loader);
                $.post(form.attr('action'), form.serialize(), function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.success) {
                        dgf.ajax.reload();
                    }
                    $('#dialog').foundation('close');
                });
            });
        });
    });
    
    $(document).on('click', '.btn-group-field-edit', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/users/group/edit_field.html', {id: $(this).attr('data-id')}, function(html){
            btn.html(txt);
            btn.prop('disabled', false);
            $('#dialog_medium').html(html);
            $('#dialog_medium').foundation('open');
            loadUploadImage();
            $('#dialog_medium').on('click', '#btn_submit_edit_group_field', function(){
                var btn = $(this);
                var txt = btn.html();
                btn.prop('disabled', true);
                btn.html(loader);
                var form = $(this).closest('form');
                form.find('.validation-error').removeClass('validation-error');
                form.find('.validation-input-error').remove();
                var data = form.serializeArray();
                data.push({name: 'data[group_id]', value: group_id});
                $.post(form.attr('action'), data, function(json){
                    
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        $.each(json.error.message, function(k,v){
                            $('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                        });
                    } else if(json.success) {
                        $('#dialog_medium').foundation('close');
                        dgf.ajax.reload();
                    }
                });
            });
        });
    });
    
    $('#btn_add_field_group').on('click', function(){
        var btn = $(this);
        var txt = btn.html();
        btn.prop('disabled', true);
        btn.html(loader);
        $.get('/users/group/edit_field.html', function(html){
            btn.prop('disabled', false);
            btn.html(txt);
            $('#dialog_medium').html(html);
            $('#dialog_medium').foundation('open');
            loadUploadImage();
            $('#dialog_medium').on('click', '#btn_submit_add_group_field', function(){
                var btn = $(this);
                var txt = btn.html();
                btn.prop('disabled', true);
                btn.html(loader);
                var form = $(this).closest('form');
                form.find('.validation-error').removeClass('validation-error');
                form.find('.validation-input-error').remove();
                var data = form.serializeArray();
                data.push({name: 'data[group_id]', value: group_id});
                $.post(form.attr('action'), data, function(json){
                    btn.prop('disabled', false);
                    btn.html(txt);
                    if(json.error) {
                        $.each(json.error.message, function(k,v){
                            $('#dialog_medium').find('#'+k).addClass('validation-error').after('<div class="validation-input-error">'+v+'</div>');
                        });
                    } else if(json.success) {
                        $('#dialog_medium').foundation('close');
                        dgf.ajax.reload();
                    }
                });
            });
        });
    });
    
    function loadUploadImage(){
        $.get('/users/group/change_type.html', {val: $('#dialog_medium').find('#type_id').val(), id: $('#dialog_medium').find('#id').val()}, function(html){
            $('#dialog_medium').find('#upload_image').html(html);
        });
    }
    
    $('#dialog_medium').on('change', '#type_id', function(){
        loadUploadImage();
    });
});